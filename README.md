# Boketto :coffee: ボケっと

A simple PHP script that generates a web page with a photo randomly picked from a specified directory.

<img src="boketto.png" alt="Boketto">

# Installation and Usage

The [Linux Photography](https://gumroad.com/l/linux-photography) book provides detailed instructions on installing and using Boketto. Get your copy at [Google Play Store](https://play.google.com/store/books/details/Dmitri_Popov_Linux_Photography?id=cO70CwAAQBAJ) or [Gumroad](https://gumroad.com/l/linux-photography).

<img src="https://scribblesandsnaps.files.wordpress.com/2016/07/linux-photography-6.jpg" width="200"/>

## Author

Dmitri Popov [dmpop@linux.com](mailto:dmpop@linux.com)

## License

The [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)
