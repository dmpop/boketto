<?php
// User-defined settings
$title = "Hanabi";  // Page title
$dir = "photos";    // Path to the directory containing photos
$md = "text.md";    // Path to the .md file
$ext = "jpeg";      // File extension of photos
$font = "Lato";     // Font

// Include parsedown.php for parsing Markdown
include('parsedown.php');

// Generate HTML
echo <<< EOT
    <!doctype html>
    <html lang="en">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
	        <title>$title</title>
	        <link href="https://unpkg.com/sakura.css/css/sakura-dark.css" rel="stylesheet" type="text/css" />
	        <link href="https://fonts.googleapis.com/css?family=$font" rel="stylesheet">
	        <link rel="shortcut icon" href="favicon.png" />
	        <style>
	            h1 { font-family:$font;font-size:2em;letter-spacing:5px; }
	            p { font-family:$font;font-size:0.9em;letter-spacing:2px; }
	        </style>
        </head>
        <body style="text-align: center";>
            <h1>$title</h1>
EOT;

// Get and parse the Markdown file
$html = file_get_contents($md);
$Parsedown = new Parsedown();
echo $Parsedown->text($html);

// Find all files with the specified extension
// Pick a random file from the resulting array
$files = glob($dir.'/*.'.$ext);
$file = array_rand($files);
$img = $files[$file];

// Extract the content of the Comment EXIF field
$exif = exif_read_data($img, 0, true);
$caption = $exif['COMMENT']['0'];

// Generate HTML
echo <<< EOT
            <img style="border-radius: 9px;"; src="$img" />
            <p style="">$caption</p>
            <input type="button" value="Reload" onClick="window.location.reload()">
EOT;
?>
